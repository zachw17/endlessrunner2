﻿using UnityEngine;
using System.Collections;

public class FollowPlayer : MonoBehaviour
{

    public Transform ObjToFollow;


    private float distance;

    void Start()
    {
        distance = gameObject.transform.position.y - ObjToFollow.transform.position.y;
    }

    void Update()
    {


        gameObject.transform.position = new Vector3(gameObject.transform.position.x, ObjToFollow.transform.position.y + distance, ObjToFollow.transform.position.z + 50f);


    }
}