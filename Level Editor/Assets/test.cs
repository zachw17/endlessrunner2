﻿

using UnityEngine;
using System.Collections;

public class test : MonoBehaviour 
{
	//if (Input.GetKeyDown(KeyCode.DownArrow) && Input.GetKeyDown(KeyCode.Keypad2))
	//if (Input.GetKeyDown(KeyCode.UpArrow) && Input.GetKeyDown(KeyCode.Keypad8))
	//if (Input.GetKeyDown(KeyCode.LeftArrow) && Input.GetKeyDown(KeyCode.Keypad4))
	//if (Input.GetKeyDown(KeyCode.RightArrow) && Input.GetKeyDown(KeyCode.Keypad6))
	
	CharacterController PlayerCtrl = new CharacterController();
	CharacterController AICtrl = new CharacterController();
	public GameObject Player;
	public GameObject AI;
	public float gravity = 20.0F;
	public float jumpSpeed = 20.0F;
	
	private Vector3 normalScale = new Vector3(1f, 1f, 1f);
	private Vector3 shrink = new Vector3(1f, 0.5f, 1f);
	private Vector3 scaleOffset = new Vector3(0, 0.25f, 0);
	
	public float forwardSpeed = 20.0f;
	public float transitionSpeed = 10.0f;
	
	private Vector3 playerPos;
	private Vector3 AIPos;
	private float distance;
	private float distance2;
	private Vector3 moveDirectionPlayer = Vector3.zero;
	private Vector3 moveDirectionAI = Vector3.zero;
	
	void Start () 
	{
		playerPos = Player.transform.position;
		AIPos = AI.transform.position;
		PlayerCtrl = Player.GetComponent<CharacterController>();
		AICtrl = AI.GetComponent<CharacterController>();
	}
	
	
	void Update () 
	{
		playerPos = Player.transform.position;
		AIPos = AI.transform.position;
		
		playerPos.x = Mathf.Lerp(playerPos.x, distance, transitionSpeed * Time.deltaTime);
		AIPos.x = Mathf.Lerp(AIPos.x, distance2, transitionSpeed * Time.deltaTime);
		
		Player.transform.position = playerPos;
		AI.transform.position = AIPos;
		
		if (Input.GetKey(KeyCode.RightArrow) && Input.GetKey(KeyCode.Keypad6))
		{
			distance -= 5.0f;
			if (distance <= -5.0f)
			{
				distance = -5.0f;
			}
		}
		
		if (Input.GetKey(KeyCode.LeftArrow) && Input.GetKey(KeyCode.Keypad4))
		{
			distance += 5.0f;
			if (distance >= 5.0f)
			{
				distance = 5.0f;
			}
		}
		
		if (Input.GetKey(KeyCode.D) && Input.GetKey(KeyCode.F))
		{
			distance2 -= 5.0f;
			if (distance2 <= -5.0f)
			{
				distance2 = -5.0f;
			}
		}
		
		if (Input.GetKey(KeyCode.A) && Input.GetKey(KeyCode.CapsLock))
		{
			distance2 += 5.0f;
			if (distance2 >= 5.0f)
			{
				distance2 = 5.0f;
			}
		}
		
		
		if (PlayerCtrl.isGrounded)
		{
			if( Player.transform.localScale == shrink)
			{ 
				Player.transform.position = Player.transform.position + scaleOffset;
				Player.transform.localScale = normalScale;
			}

			if (Input.GetKeyDown(KeyCode.UpArrow) && Input.GetKeyDown(KeyCode.Keypad8))
			{
				moveDirectionPlayer.y = jumpSpeed;
			}
			
			if (Input.GetKeyDown(KeyCode.DownArrow) && Input.GetKeyDown(KeyCode.Keypad2))
			{
				Player.transform.localScale = shrink;
			}
		}
		
		if(AICtrl.isGrounded)
		{
			if (AI.transform.localScale == shrink)
			{
				AI.transform.position = AI.transform.position + scaleOffset;
				AI.transform.localScale = normalScale;
			}
			
			if (Input.GetKeyDown(KeyCode.S) && Input.GetKeyDown(KeyCode.X))
			{
				
				AI.transform.localScale = shrink;
			}

			if (Input.GetKeyDown(KeyCode.W) && Input.GetKeyDown(KeyCode.Q))
			{
				moveDirectionAI.y = jumpSpeed;
				
			}
		}
		
		moveDirectionPlayer.y -= gravity * Time.deltaTime;
		moveDirectionPlayer.z = forwardSpeed;
		
		moveDirectionAI.y -= gravity * Time.deltaTime;
		moveDirectionAI.z = forwardSpeed;
		
		PlayerCtrl.Move(moveDirectionPlayer * Time.deltaTime);
		AICtrl.Move (moveDirectionAI * Time.deltaTime);
		
	}
}